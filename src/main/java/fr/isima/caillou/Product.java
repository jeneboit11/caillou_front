package fr.isima.caillou;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class Product implements Serializable {
    private long id;
    private String name;
    private int nutritionScore;
    private ArrayList<String> qualities = new ArrayList<>();
    private ArrayList<String> faults = new ArrayList<>();
    private String color;
    private String classe;

    public Product() {

    }

    public Product(String name, int nutritionScore, ArrayList<String> qualities, ArrayList<String> faults, String classe, String color) {
        this.name = name;
        this.nutritionScore = nutritionScore;
        this.qualities = qualities;
        this.faults = faults;
        this.classe = classe;
        this.color = color;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNutritionScore() {
        return nutritionScore;
    }

    public void setNutritionScore(int nutritionScore) {
        this.nutritionScore = nutritionScore;
    }

    public ArrayList<String> getQualities() {
        return qualities;
    }

    public void setQualities(ArrayList<String> qualities) {
        this.qualities = qualities;
    }

    public ArrayList<String> getFaults() {
        return faults;
    }

    public void setFaults(ArrayList<String> faults) {
        this.faults = faults;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getClasse() {
        return classe;
    }

    public void setClasse(String classe) {
        this.classe = classe;
    }

    static public Product getProduct(HttpResponse<JsonNode> response) {
        JSONObject body = response.getBody().getObject();
        String name = body.getString("name");
        int nutritionScore = body.getInt("nutritionScore");
        JSONArray jqualities = body.getJSONArray("qualities");
        ArrayList<String> qualities = new ArrayList<>();
        for (int i=0;i<jqualities.length();i++){
            qualities.add(jqualities.getString(i));
        }
        JSONArray jfaults = body.getJSONArray("faults");
        ArrayList<String> faults = new ArrayList<>();
        for (int i=0;i<jfaults.length();i++){
            faults.add(jfaults.getString(i));
        }
        String classe = body.getString("classe");
        String color = body.getString("color");
        return new Product(name, nutritionScore, qualities, faults, classe, color);
    }
}
