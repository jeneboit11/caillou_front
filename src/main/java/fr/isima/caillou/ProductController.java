package fr.isima.caillou;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import com.mashape.unirest.http.Unirest;

@Controller
public class ProductController {
    @Value("${url}")
    private String url;

    @GetMapping("/product")
    public String productForm(Model model) {
        model.addAttribute("product", new Product());
        return "product";
    }

    @PostMapping("/product")
    public String productSubmit(@ModelAttribute Product product) throws UnirestException {
        HttpResponse<JsonNode> response = Unirest.get(url + "/api/v1/products/" + product.getId()).asJson();
        Product p = Product.getProduct(response);
        product.setName(p.getName());
        product.setNutritionScore(p.getNutritionScore());
        product.setQualities(p.getQualities());
        product.setFaults(p.getFaults());
        product.setColor(p.getColor());
        product.setClasse(p.getClasse());
        return "result";
    }

}
